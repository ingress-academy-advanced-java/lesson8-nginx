package com.example.dockerdemo.service.impl;

import com.example.dockerdemo.dto.request.UserRequest;
import com.example.dockerdemo.dto.response.UserResponse;
import com.example.dockerdemo.model.User;
import com.example.dockerdemo.repository.UserRepository;
import com.example.dockerdemo.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(UserRequest userRequest) {
        User user = new User();
        user.email = userRequest.getEmail();
        user.password = userRequest.getPassword();
        user.username = userRequest.getUsername();
        userRepository.save(user);

        return user;
    }

    @Override
    public UserResponse getById(String id) {
        User user = userRepository.findById(id).orElseThrow();
        return UserResponse.builder()
                .username(user.username)
                .email(user.email)
                .build();

    }

    @Override
    public List<UserResponse> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserResponse> userResponses = new ArrayList<>();
        users.forEach(user -> {
            userResponses.add(
                    UserResponse.builder()
                            .email(user.email)
                            .username(user.username)
                            .build()
            );
        });

        return userResponses;
    }
}
