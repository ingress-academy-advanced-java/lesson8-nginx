package com.example.dockerdemo.service;


import com.example.dockerdemo.dto.request.UserRequest;
import com.example.dockerdemo.dto.response.UserResponse;
import com.example.dockerdemo.model.User;

import java.util.List;

public interface UserService {
    User save(UserRequest userRequest);
    UserResponse getById(String id);
    List<UserResponse> getAllUsers();
}
