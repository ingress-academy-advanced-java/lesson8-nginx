package com.example.dockerdemo.repository;

import com.example.dockerdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
