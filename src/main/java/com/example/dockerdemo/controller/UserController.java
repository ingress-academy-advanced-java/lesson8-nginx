package com.example.dockerdemo.controller;

import com.example.dockerdemo.dto.request.UserRequest;
import com.example.dockerdemo.dto.response.UserResponse;
import com.example.dockerdemo.model.User;
import com.example.dockerdemo.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> getUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("{id}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable String id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody UserRequest userRequest) {
       User user =  userService.save(userRequest);
        return ResponseEntity.created(URI.create("Created")).body(user);
    }
}
